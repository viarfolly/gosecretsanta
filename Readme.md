###### **README**

Почтовый Secret Santa

1. Запускаем Secret Santa `./app`
2. Вызывем метод `POST` для корня сервиса, с телом `JSON` на `http://localhost:8080`
3. Prof

Пример запроса:

```JSON
{
  "time": "Время встречи",
  "place": "Место встречи",
  "comment": "Добавить комментарий к встречи",
  "cost": 3456.99, //Бюджет на подарки
  "count": 3, //Количество подарков
  "people": [
    {
      "name": "Кому дарить 1",
      "email": "user1@example.com",
      "wish_list": "Хочу подарок 1, подраок 2"
    },
    {
      "name": "Кому дарить 2",
      "email": "user2@example.com",
      "wish_list": "Хочу подарок 1, подраок 2"
    },
    {
      "name": "Кому дарить 3",
      "email": "user3@example.com",
      "wish_list": "Хочу подарок 1, подраок 2"
    }
  ]
}
```

