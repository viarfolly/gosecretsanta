package main

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOnePersonCannotPlaySecretSanta(t *testing.T) {
	assertt := assert.New(t)
	service := service{}

	item := serviceItem{
		Count: 1,
		People: []person{
			{Name: "user1"},
		},
	}

	_, err := service.do(&item)
	assertt.Equal(" Invalid arguments", err.Error())
}

func TestNotEnoughPersons(t *testing.T) {
	assertt := assert.New(t)
	service := service{}

	item := serviceItem{
		Count: 3,
		People: []person{
			{Name: "user0"},
			{Name: "user1"},
			{Name: "user2"},
		},
	}

	_, err := service.do(&item)
	assertt.Equal(" Invalid arguments", err.Error())
}

func recipientsAreDistributedCorrectly(t *testing.T, personsCount int, giftsLimit int) {
	assertt := assert.New(t)
	service := service{}
	var persons []person
	for i := 0; i < personsCount; i++ {
		persons = append(persons, person{strconv.Itoa(i), "", ""})
	}
	item := serviceItem{
		Count:  giftsLimit,
		People: persons,
	}

	for n := 0; n < 500; n++ {
		var gifts []int
		for i := 0; i < personsCount; i++ {
			gifts = append(gifts, 0)
		}

		res, err := service.do(&item)

		assertt.Empty(err)
		assertt.Equal(personsCount, len(res))
		for i := 0; i < personsCount; i++ {
			assertt.Equal(giftsLimit, len(res[i].to))
			itemsAreDifferent(res[i].to)
			for j := 0; j < giftsLimit; j++ {
				assertt.NotEqual(res[i].from, res[i].to[j])
				name, _ := strconv.Atoi(res[i].to[j].Name)
				gifts[name]++
			}
		}
		for i := 0; i < personsCount; i++ {
			assertt.Equal(giftsLimit, gifts[i])
		}
	}
}

func itemsAreDifferent(items []person) (different bool) {
	len := len(items)
	for i := 0; i < len; i++ {
		for j := 0; j < len; j++ {
			if i != j && items[i] == items[j] {
				return false
			}
		}
	}
	return true
}

func TestRecipientsAreDistributedCorrectly(t *testing.T) {
	recipientsAreDistributedCorrectly(t, 5, 2)
	recipientsAreDistributedCorrectly(t, 5, 3)
	recipientsAreDistributedCorrectly(t, 6, 2)
	recipientsAreDistributedCorrectly(t, 6, 3)
	recipientsAreDistributedCorrectly(t, 7, 6)
	recipientsAreDistributedCorrectly(t, 8, 7)
	recipientsAreDistributedCorrectly(t, 9, 1)
}
