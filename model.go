package main

type service struct {
	helpers
}

type serviceItem struct {
	Time    string   `json:"time"`
	Place   string   `json:"place"`
	Comment string   `json:"comment"`
	Cost    float64  `json:"cost"`
	Count   int      `json:"count"`
	People  []person `json:"people"`
}

type person struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	WishList string `json:"wish_list"`
}

type letter struct {
	to      string
	subject string
	body    string
}

type recipients struct {
	from person
	to   []person
}

type helpers interface {
	do(item *serviceItem) (req []recipients, err error)
	sender(request *letter) (err error)
	makeBody(peopleTo person, peoples []person, cost float64) (body string, err error)
}
