package main

import (
	"errors"
	"fmt"
	"github.com/kataras/golog"
	"github.com/matcornic/hermes/v2"
	"math/rand"
	"strings"
	"time"

	"gopkg.in/gomail.v2"

	"github.com/kataras/iris"
	"github.com/kataras/iris/context"
	"github.com/kataras/iris/middleware/logger"
	"github.com/kataras/iris/middleware/recover"
	"github.com/spf13/viper"
)

var (
	config = viper.New()
	log    *golog.Logger
)

func main() {

	app := iris.New()

	log = app.Logger()

	config.SetConfigName("config") // name of config file (without extension)
	config.AddConfigPath(".")
	err := config.ReadInConfig()
	if err != nil {
		log.Fatalf("Fatal error config file: %s \n", err)
	}

	app.Logger().SetLevel("debug")
	app.Use(recover.New())
	app.Use(logger.New())
	app.Handle("POST", "/", handlerRootPOST)
	_ = app.Run(iris.Addr(":8080"), iris.WithoutServerError(iris.ErrServerClosed))
}

func handlerRootPOST(context context.Context) {

	item := serviceItem{}

	err := context.ReadJSON(&item)
	if err != nil {
		context.StatusCode(iris.StatusBadRequest)
		_, _ = context.WriteString(err.Error())
		return
	}

	service := service{}

	recipients, err := service.do(&item)
	if err != nil {
		context.StatusCode(iris.StatusInternalServerError)
		_, _ = context.WriteString(err.Error())
		return
	}

	for _, v := range recipients {
		body, err := service.makeBody(v.from, v.to, item.Cost)
		if err != nil {
			context.StatusCode(iris.StatusInternalServerError)
			_, _ = context.WriteString(err.Error())
			return
		}

		if v.from.Email == "" {
			log.Infof("Manual sender! Привет, %v! Получатели подарка: %v", v.from.Name, strings.Join(func() []string {
				var r []string
				for _, v := range v.to {
					r = append(r, v.Name)
				}
				return r
			}(), ","))
			continue
		}

		err = service.sender(&letter{
			to:      v.from.Email,
			subject: "Secret Santa",
			body:    body,
		})
		if err != nil {
			context.StatusCode(iris.StatusInternalServerError)
			_, _ = context.WriteString(err.Error())
			return
		}
	}

	context.StatusCode(iris.StatusOK)
	_, _ = context.WriteString("It's done!))")

	return
}

func shuffle(items []person) {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	for n := len(items); n > 0; n-- {
		randIndex := r.Intn(n)
		items[n-1], items[randIndex] = items[randIndex], items[n-1]
	}
}

func (s *service) do(item *serviceItem) (result []recipients, err error) {
	persons := len(item.People)
	giftsLimit := item.Count
	if persons <= 1 || giftsLimit > persons-1 {
		return nil, errors.New(" Invalid arguments")
	}

	shuffle(item.People)
	for index, from := range item.People {
		var to []person
		for i := 1; i <= giftsLimit; i++ {
			j := (index + i) % persons
			to = append(to, item.People[j])
		}
		result = append(result, recipients{
			from: from,
			to:   to,
		})
	}
	return
}

func (s *service) sender(request *letter) (err error) {

	m := gomail.NewMessage()
	m.SetHeader("From", config.GetString("email"))
	m.SetHeader("To", request.to)
	m.SetHeader("Subject", request.subject)
	m.SetBody("text/html", request.body)

	d := gomail.NewDialer(config.GetString("server"), config.GetInt("port"), config.GetString("email"), config.GetString("pass"))

	err = d.DialAndSend(m)

	return
}

func (s *service) makeBody(from person, to []person, cost float64) (body string, err error) {

	h := hermes.Hermes{
		Product: hermes.Product{
			Name:      "Secret Santa",
			Logo:      "https://c7.uihere.com/files/327/743/656/santa-claus-secret-santa-gift-christmas-child-santa-claus-portrait-illustration.jpg",
			Copyright: "Copyright © 2019 ViArFolly. All rights reserved.",
		},
	}

	table := hermes.Table{}
	wishList := false
	for _, recipient := range to {
		wishList = wishList || recipient.WishList != ""
	}
	if wishList {
		for _, recipient := range to {
			table.Data = append(table.Data, []hermes.Entry{
				{Key: "Имя", Value: recipient.Name},
				{Key: "Wish list", Value: recipient.WishList},
			})
		}
	} else {
		for _, recipient := range to {
			table.Data = append(table.Data, []hermes.Entry{
				{Key: "Имя", Value: recipient.Name},
			})
		}
	}

	recipients := "Твой получатель подарка:"
	if len(to) > 1 {
		recipients = "Твои получатели подарка:"
	}

	body, err = h.GenerateHTML(hermes.Email{
		Body: hermes.Body{
			Greeting: "Привет,",
			Name:     from.Name,
			Intros: []string{
				"Добро пожаловать в Secret Santa!",
				"Примерный бюджет подарка - " + fmt.Sprintf("%.2f", cost) + " рублей.",
				recipients,
			},
			Table:     table,
			Signature: "До встречи",
		},
	})

	return
}
